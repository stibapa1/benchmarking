package cz.esw.benchmark;

import java.util.Random;

/**
 * @author Marek Cuchý
 */
public class Main {

	public static final int SEED = 0;
	public static final int MAX = 10; // value upper bound of the randomly generated matrices

	//matrices dimensions
	public static final int N = 968;
	public static final int M = 333;
	public static final int P = N;

	public static void main(String[] args) {
		Random rnd = new Random(SEED);

		double[][] a = MatrixUtils.generateMatrix(rnd, N, M, MAX);
		double[][] b = MatrixUtils.generateMatrix(rnd, M, P, MAX);
		double[] a1D = MatrixUtils.to1D(a);
		double[] b1D = MatrixUtils.to1D(b);

		for (int i = 0; i < 30; i++) {
			long t1 = System.nanoTime();
			double[][] c = MatrixUtils.multiply(a, b);
//			double[][] cTrans = MatrixUtils.multiplyTrans(a, b);
//			double[] c1D = MatrixUtils.multiply1D(a1D, b1D, N, M, P);
			long t2 = System.nanoTime();
			System.out.println("TIME: " + (t2 - t1) / 1000000 + "ms");
		}
	}

}
